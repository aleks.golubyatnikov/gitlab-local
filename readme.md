### Docker & Docker-compose installation:
```
sudo apt install docker.io
sudo usermod -aG docker $USER

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

docker --version
docker-compose --version
```

### Making configuration changes:
```
# .env file in root folder:
GITLAB_HOME=/srv/gitlab
GITLAB_HOSTNAME=10.106.65.24
GITLAB_DOMAIN=astra-server-01
```

### Creating the necessary directories:
```
mkdir -p /srv/gitlab/config /srv/gitlab/logs /srv/gitlab/data  
```

### Run Gitlab-server:
```
cd ./gitlab-local
docker-compose up -d

#view root password
cat /srv/config/gitlab/initial_root_password
```